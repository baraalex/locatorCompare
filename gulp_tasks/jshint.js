const gulp = require('gulp');
const jshint = require('gulp-jshint');

const conf = require('../conf/gulp.conf');

gulp.task('jshint', jshintScripts);

function jshintScripts() {
    return gulp.src([conf.path.src('app/**/!(*.html|*.spec|*.mock).js'), conf.path.src('*.js')])
        .pipe(jshint())
        .pipe(jshint.reporter('default', {
            verbose: true
        }));
}
