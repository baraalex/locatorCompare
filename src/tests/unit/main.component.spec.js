/* jshint ignore:start */
describe('mainComponentController', function () {
    //scopes
    var $rootScope, $scope;
    //angular services
    var $httpBackend, $q, $log;
    //app services, factories,...
    var mainService;
    //variables
    var crtlInject, ctrl, def;


    beforeEach(module('app'));

    beforeEach(angular.mock.inject(function ($componentController, _$rootScope_, _$httpBackend_, _$log_, _$q_, _mainService_) {

        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;
        $q = _$q_;

        $log = _$log_;
        mainService = _mainService_;

        crtlInject = {
            $scope: $scope,
            $log: $log,
            mainService: mainService
        };

        bindings = {};

        ctrl = $componentController('mainComponent', crtlInject, bindings);

        spyOn($log, 'error');
    }));

    /*** component controller definition ***/
    describe("main component", function () {
        Then("Should component controller be defined", function () {
            expect(ctrl).toBeDefined();
        });
    });

    /*** onInit ***/
    describe("$onInit", function () {
        var responseLocations;
        beforeEach(function () {
            responseLocations = {
                data: angular.copy(locationsMock)
            };

            spyOn(mainService, 'getLocations').and.callFake(function () {
                def = $q.defer();
                if (mainCallSuccess) {
                    def.resolve(responseLocations);
                } else {
                    def.reject('ERRORS');
                }
                return def.promise;
            });
        });

        describe(' Error getting locations', function () {
            Given(function () {
                mainCallSuccess = false;
            })
            When(function () {
                ctrl.$onInit();
                $scope.$digest();
            });

            Then("Should call mainService.getLocations, log error and set the variables", function () {
                expect(mainService.getLocations).toHaveBeenCalled();
                expect($log.error).toHaveBeenCalledWith('ERROR GETTING LOCATIONS COMPARE', 'ERRORS');
                expect(ctrl.error).toBe(true);
            });

        });

        describe('Get locations OK', function () {
            Given(function () {
                mainCallSuccess = true;
            })
            When(function () {
                ctrl.$onInit();
                $scope.$digest();
            });

            Then("Should call mainService.getLocations and set locations", function () {
                expect(mainService.getLocations).toHaveBeenCalled();
                expect(ctrl.locations).toEqual(locationsFormattedMock);
                expect(ctrl.error).toBe(false);
            });

        });
    });
});
