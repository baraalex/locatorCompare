angular.module("config", [])
    .constant("environmentConfig", {
        apiURL: "http://localhost:1337/",
        compare1: {
            color: "rgb(236, 151, 31)",
            color05: "rgba(236, 151, 31, .5)"
        },
        compare2: {
            color: "rgb(51, 122, 183)",
            color05: "rgba(51, 122, 183, .5)"
        },
        reference: {
            color: "rgb(201, 48, 44)",
            color05: "rgba(201, 48, 44, .5)"
        }
    });
