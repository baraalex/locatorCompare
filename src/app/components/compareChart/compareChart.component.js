(function () {

    // Usage:
    //  Component to show comparision chart of locations
    // Bindings
    // - locations : object with locations to show comparision
    angular
        .module('app')
        .component('compareChart', {
            templateUrl: 'app/components/compareChart/compareChart.html',
            controller: compareChartController,
            controllerAs: '$ctrl',
            bindings: {
                locations: '='
            }
        });

    function compareChartController(environmentConfig) {
        var vm = this;

        vm.showHide = showHide;
        vm.download = download;
        vm.downloadFile = false;

        vm.$onInit = function () {

            vm.data = {
                labels: [],
                pointRadius: 0,
                datasets: []
            };

            var options = {
                maintainAspectRatio: true,
                spanGaps: false,
                elements: {
                    line: {
                        tension: 0.000001
                    }
                },
                layout: {
                    padding: 50
                },
                legend: {
                    display: false
                },
                title: {
                    display: false
                },
                scale: {
                    ticks: {
                        display: false,
                        suggestedMin: 0
                    },
                    angleLines: {
                        color: '#5aadbb',
                        lineWidth: 3
                    },
                    gridLines: {
                        borderDashOffset: 20,
                    },
                    pointLabels: {
                        fontColor: 'rgba(54, 162, 235, 0.5)',
                        fontFamily: '\'Open Sans\', sans-serif',
                        fontSize: 16
                    }
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                }
            };

            _setData(vm.data);

            vm.chart = new Chart('chart-compare', {
                type: 'radar',
                data: vm.data,
                options: options
            });
        };

        /**
         * method to set data to download and download it
         * 
         */
        function download() {
            vm.dataDownload = angular.copy(vm.locations);

            for (var index = 0; index < vm.dataDownload.length; index++) {
                var element = vm.dataDownload[index];
                delete element.compare;
                delete element.$$hashKey;
            }

            vm.dataDownload = JSON.stringify(vm.dataDownload);

            vm.downloadFile = true;
        }


        /**
         * method to hide or show a dataset on chart
         * 
         * @param {integer} index - index of dataset to show or hide 
         */
        function showHide(index) {
            vm.data.datasets[index].hidden = !vm.data.datasets[index].hidden;
            vm.chart.update();
        }


        /**
         * method to set the chart dataset and labels
         * 
         * @param {Object} data - data to add datasets and labels
         */
        function _setData(data) {
            var keys = Object.keys(vm.locations[0].variables.indexes);

            for (var index = 0; index < keys.length; index++) {
                var label = keys[index];
                var labelValue;

                switch (label) {
                    case 'population':
                        labelValue = 'Población';
                        break;
                    case 'unemployment':
                        labelValue = 'Desempleo';
                        break;
                    case 'commercial_activity':
                        labelValue = 'Avtividad comercial';
                        break;
                    case 'wealth':
                        labelValue = 'Riqueza';
                        break;
                    case 'traffic':
                        labelValue = 'Trafico peatonal';
                        break;
                    case 'foreigners':
                        labelValue = 'Extranjeros';
                        break;
                    case 'dependency_rate':
                        labelValue = 'Tasa de dependencia';
                        break;

                    default:
                        labelValue = label;
                        break;
                }

                data.labels.push(labelValue);
            }

            for (var i = 0; i < vm.locations.length; i++) {
                var location = vm.locations[i];
                data.datasets.push(_setDataSet(location, keys));

            }
        }


        /**
         * method to set dataset information
         * 
         * @param {Object} location - data to set
         * @param {Array} keys - data keys to set into dataset
         * @returns - dataset formmated
         */
        function _setDataSet(location, keys) {
            var set;

            if (location.variables.is_reference) {
                set = {
                    backgroundColor: environmentConfig.reference.color05,
                    pointBackgroundColor: environmentConfig.reference.color,
                    borderColor: environmentConfig.reference.color,
                    data: [],
                    label: 'Reference area'
                };
            } else if (location.compare === 1) {
                set = {
                    backgroundColor: environmentConfig.compare1.color05,
                    pointBackgroundColor: environmentConfig.compare1.color,
                    borderColor: environmentConfig.compare1.color,
                    data: [],
                    label: 'Compared area ' + location.compare,
                    fill: 'zero'
                };
            } else if (location.compare === 2) {
                set = {
                    backgroundColor: environmentConfig.compare2.color05,
                    pointBackgroundColor: environmentConfig.compare2.color,
                    borderColor: environmentConfig.compare2.color,
                    data: [],
                    label: 'Compared area ' + location.compare,
                    fill: 'zero'
                };
            }

            for (var index = 0; index < keys.length; index++) {
                var key = keys[index];
                set.data.push(location.variables.indexes[key]);
            }

            return set;
        }
    }
})();
