(function() {
    'use strict';
    angular
        .module('app')
        .directive('downloadFile', downloadFile);

    /* @ngInject */
    function downloadFile($timeout) {
        // Usage: directive to download csv generated
        var directive = {
            link: link,
            restrict: 'EA',
            replace: true,
            template: '<a id="downloadCsv" class="ng-hide"></a>',
            scope: {
                data: "=",
                fileName: "@",
                download: "=",
                extension: "="
            }
        };
        return directive;

        function link(scope, element) {
            download();

            function download() {
                var blob, type, fileName;

                if (scope.extension === 'csv') {
                    type = "application/csv;charset=iso-8859-1";
                } else if (scope.extension === 'xlsx') {
                    type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=iso-8859-1";
                } else if (scope.extension === 'xml') {
                    type = "application/xml;charset=iso-8859-1";
                } else if (scope.extension === 'json') {
                    type = "application/json";
                }

                blob = new Blob([scope.data], {
                    type: type
                });

                fileName = scope.fileName + "." + scope.extension;

                if (navigator.appVersion.toString().indexOf('.NET') > 0 || navigator.appVersion.toString().indexOf('Edge') > 0) {
                    window.navigator.msSaveBlob(blob, fileName);
                    scope.download = false;
                } else {
                    $timeout(function() {
                        element.attr({
                            "download": fileName,
                            "href": URL.createObjectURL(blob),
                            "target": "_blank"
                        })[0].click();
                        scope.download = false;
                    });
                }
            }
        }
    }
})();
