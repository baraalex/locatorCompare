# Geoblink Comparaltor Locations
## Requirements ##

- Install latest [Node JS](https://nodejs.org/en/download/) higher than v.4.x.x
- Install [bower](https://bower.io/) `npm install -g bower`
- Install [gulp cli](http://gulpjs.com/) `npm install -g gulp-cli`

## Install Dependencies ##

- npm and bower dependencies run `npm install && bower install`

## Launch server ##

- `node server/server.js` to launch server with mocked data

## Gulp tasks ##

- `gulp clean` to clean paths
- `gulp build` to build dist folder
- `gulp serve` to launch a browser sync server on your source files
- `gulp serve:dist` to launch a server on your optimized application
- `gulp test` to launch your unit tests with Karma

## Deploy in all environments ##

1. Do 'Setup' and 'Install Dependencies' steps
3. run `gulp build` to generate dist folder and point app url to that folder

## Plugins ##

- [Chart.js](http://www.chartjs.org/)


## REASONS OF USE ##
I use angular 1 instead of other frontend framework because I feel more confortable and confident with it because I have used it every day for the last 2 years on my daily work. I think its a good choise because its a MV* framework that provides me with almost all the tools the I needed for this test, with the exception of charts. For charts I use Chart.js becuse I have tested it in personal test and I already know how it work better than others like 3d.js.
