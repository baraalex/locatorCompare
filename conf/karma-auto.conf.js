const conf = require('./gulp.conf');
const listFiles = require('./karma-files.conf');

module.exports = function (config) {
  const configuration = {
    basePath: '../',
    singleRun: false,
    autoWatch: true,
    colors: true,
    logLevel: 'INFO',
    junitReporter: {
      outputDir: 'test-reports'
    },
    browsers: [
      'PhantomJS'
    ],
    frameworks: [
      'phantomjs-shim',
      'jasmine-given',
      'jasmine',
      'angular-filesort'
    ],
    files: listFiles(),
    preprocessors: {
      [conf.path.src('**/*.html')]: [
        'ng-html2js'
      ],
      [conf.path.src('**/!(*.html|*.spec|*.mock).js')]: ['coverage']
    },
    ngHtml2JsPreprocessor: {
      stripPrefix: `${conf.paths.src}/`,
      moduleName: 'app'
    },
    angularFilesort: {
      whitelist: [
        conf.path.tmp('**/!(*.html|*.spec|*.mock).js')
      ]
    },
    plugins: [
      require('karma-jasmine'),
      require('karma-jasmine-given'),
      require('karma-coverage'),
      require('karma-html-reporter'),
      require('karma-phantomjs-launcher'),
      require('karma-phantomjs-shim'),
      require('karma-ng-html2js-preprocessor'),
      require('karma-angular-filesort')
    ],
    reporters: ['progress', 'coverage'],
    coverageReporter: {
        type: 'lcov',
        dir: 'tests/reports', // where to store the report
        subdir: 'coverage',
        includeAllSources: true // Esto es para que aparezcan todas las fuentes (incluidas las que no tiene conbertura alguna) en el report. En la siguiente versión de karma-coverage saldrá corregido: https://github.com/karma-runner/karma-coverage/pull/131/commits
    }
  };

  config.set(configuration);
};
